[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Android](https://img.shields.io/badge/designed%20for-Android-green.svg?logo=android&color=a4c639)](https://www.android.com)
[![Java](https://img.shields.io/badge/using-Java-blue.svg?logo=java&color=007396)](http://www.oracle.com/technetwork/java/index.html)

# TTS Tool
It's a simple app for accessing the text to speech engines, which are
preinstalled on the most devices.

## How can I contribute?
I'm still searching for someone, who is able to design nice icons for my
projects.

## Other
Feel free to contact me, if there are any (aditional) questions.